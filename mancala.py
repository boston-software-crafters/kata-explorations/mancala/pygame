import pygame


class Cup(object):
    def __init__(self, stones=4, next=None, x=0, y=0):
        self.stones = stones
        self.nextCup = next
        self.x = x
        self.y = y

    def getStoneCount(self):
        return self.stones

    def setNext(self, cup):
        self.nextCup = cup

    def getNext(self):
        return self.nextCup

    def addStone(self):
        self.stones += 1

    def sew(self, numberOfStones):
        if numberOfStones > 0:
            self.stones += 1
        if numberOfStones - 1 > 0:
            self.nextCup.sew(numberOfStones - 1)


class Pit(Cup):
    def select(self):
        self.nextCup.sew(self.stones)
        self.stones = 0

    def __str__(self):
        return "(" + str(self.stones) + ")"

    def draw(self):
        pygame.draw.circle(screen, (225, 200, 0), (self.x, self.y), (5 * self.stones) + 5)


class Store(Cup):
    def __init__(self, stones=0, next=None, x=0, y=150):
        self.stones = stones
        self.nextCup = next
        self.x = x
        self.y = y

    def __str__(self):
        return "(  " + str(self.stones) + "  )"

    def draw(self):
        pygame.draw.ellipse(screen, (225, 200, 0), (self.x, self.y, 50, 100))


class Board(object):
    def __init__(self):
        self.sides = [[], []]
        self.sides[0] = self.initializeSide(y=150)
        self.sides[1] = self.initializeSide(y=250)
        self.getStore(0).setNext(self.getCup(1, 0))
        self.getStore(1).setNext(self.getCup(0, 0))
        self.sides[0][-1].x = 100
        self.sides[1][-1].x = 550

    def initializeSide(self, pitsPerSide=6, y=50):
        cups = []
        for i in range(0, pitsPerSide):
            cups.append(Pit(x=i * 60 + 200, y=y))
        cups.append(Store())
        for i in range(0, pitsPerSide):
            cups[i].setNext(cups[i + 1])
        return cups

    def getCup(self, sideNumber, index):
        return self.sides[sideNumber][index]

    def getSide(self, sideNumber):
        return self.sides[sideNumber]

    def getStore(self, sideNumber):
        return self.getSide(sideNumber)[-1]

    def select(self, sideNumber, index):
        self.getCup(sideNumber, index).select()

    def draw(self):
        for cup in self.sides[0]:
            cup.draw()
        for cup in self.sides[1]:
            cup.draw()

    def __str__(self):
        board = "\n  " + str(self.getStore(1)) + "\n"
        for i in range(0, 6):
            board += str(i) + "-" + str(self.sides[0][i]) + " " + str(self.sides[1][5 - i]) + "-" + str(5 - i) + "\n"
        return board + "  " + str(self.getStore(0)) + "\n"


board = Board()

pygame.init()

SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600
screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))

pygame.display.set_caption("Mancala")

running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

    board.draw()
    pygame.display.update()

pygame.quit()
